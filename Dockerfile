FROM fedora:latest
RUN dnf -y install centos-packager git jq mock rpmdevtools xz python-pip \
        && dnf clean all
